import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FightersComponent} from './fighters/fighters.component';
import {PlayerPageComponent} from './player-page/player-page.component';

const routes: Routes = [
  {path: '', redirectTo: 'fighters', pathMatch: 'full'},
  {path: 'fighters', component: FightersComponent},
  {path: 'fighter/:id', component: PlayerPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
