import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FightersService {

  constructor(private http: HttpClient) {
  }

  getAllFighters(): Observable<any> {
    return this.http.get('//localhost:8080/fighters');
  }

  getFighter(id): Observable<any> {
    return this.http.get('//localhost:8080/fighter/' + id);
  }

  getAllGames(): Observable<any> {
    return this.http.get('//localhost:8080/gamesUniverse');
  }

  getFightersByGameType(gameName): Observable<any> {
    return this.http.get('//localhost:8080/gameTypes/' + gameName);
  }

  getAllFighterImages(): Observable<any> {
    return this.http.get('//localhost:8080/fighterImages');
  }
}
