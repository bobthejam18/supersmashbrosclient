import { Component, OnInit } from '@angular/core';
import { FightersService } from '../Services/fighters.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-player-page',
  templateUrl: './player-page.component.html',
  styleUrls: ['./player-page.component.css']
})
export class PlayerPageComponent implements OnInit {
  fighterCarousel: Array<any>;
  fighter: Observable<any>;
  fighterImages = [];
  fighterImage: string;
  fighterDescription: Array<any>;
  selectedId: number;
  index = 0;
  constructor(private fighterService: FightersService, private route: ActivatedRoute) { }

  ngOnInit() {

    this.selectedId = +this.route.snapshot.paramMap.get('id');

    this.fighterService.getAllFighters().subscribe(fighterCarousel => {
      this.fighterCarousel = fighterCarousel; });

    this.updateFighter(this.selectedId);
  }

  normalizeIndex(): void {
    if (this.index > this.fighterImages.length - 1) {
      this. index = 0;
    }
  }

  changeImage(): void {
    this.index++;
    this.normalizeIndex();
    this.fighterImage = this.fighterImages[this.index];
  }

  // playerDescription(id): void {
  //   this.fighterService.getFighter(id).subscribe(fighter => {
  //     this.fighterDescription = fighter.description.toString().split(',');
  //     this.fighter = fighter;
  //     this.index = this.fighterDescription.length;
  //     this.fighterDescription = this.fighterDescription[this.index - 1];
  //   })
  // }

  updateFighter(id): void {
    this.fighterService.getFighter(id).subscribe(fighter => {
      this.fighterImages = fighter.image.toString().split(',');
      this.fighter = fighter;
      this.index = this.fighterImages.length;
      this.fighterImage = this.fighterImages[this.index - 1];
    });

  }
}
